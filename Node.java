import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Node {
	private Node parentNode; // parent node
	private List<OthelloMove> moves = new ArrayList<OthelloMove>();
	private List<Node> children = new ArrayList<Node>();

	private OthelloMove actionMove; // action that led us to the state

	private int numVisited = 0; // no of times visited
	
	private OthelloState boardState;
	private int score = 0;

	public Node(OthelloState state) {
		boardState = state;
		moves = boardState.generateMoves();
		if (moves.isEmpty()) {
			moves.add(null);
		}
		actionMove = null;
		parentNode = null;
		children = new ArrayList<Node>();
		numVisited = 0;
		score = 0;
	}

	public Node(Node parent, OthelloMove move) {
		this(parent.boardState.applyMoveCloning(move));
		parentNode = parent;
		actionMove = move;
	}

	public boolean noMoves() {
		return moves.isEmpty();
	}

	// create list of children moves
	public void generateMoves() {
		List<OthelloMove> moves = boardState.generateMoves();
		if (!moves.isEmpty()) {
			this.moves = moves;
		}
	}
	

	public boolean addNextChild() {
		if (children.size() < moves.size()) {
			children.add(new Node(this, moves.get(children.size())));
			return true;
		}

		return false;
	}

	public boolean gameOver() {
		return boardState.gameOver();
	}
// increments in 1 the number of times "node" has been visited, and updates the average score in
	//"node" with the value "score". If "node" has a parent, then it calls "backup(node.parent,score)
	public void backup(int score1) {
		numVisited++;
		score += score1;
		if (parentNode != null) {
			parentNode.backup(score1);
		}
	}

	//if next player to move in node is PLAYER1, it returns the child with the maximum average
	//score, if the next player to move in the node is PLAYER2, then it returns the child with the minimum average score
	public Node bestChild() {
		int index = 0;
		if (boardState.nextPlayer() == boardState.PLAYER1) {
			double max = children.get(0).getAverageScore();
			for (int i = 1; i < children.size(); i++) {
				double next = children.get(i).getAverageScore();
				if (next > max) {
					max = next;
					index = i;
				}
			}
		} else {
			double min = children.get(0).getAverageScore();
			for (int i = 1; i < children.size(); i++) {
				double next = children.get(i).getAverageScore();
				if (next < min) {
					min = next;
					index = i;
				}
			}
		}

		return children.get(index);
	}

	public Node randomChild(Random rand) {
		return children.get(rand.nextInt(children.size()));
	}

	public OthelloState stateClone() {
		return boardState.clone();
	}

	private double getAverageScore() {
		return (double) score / numVisited;
	}

	public Node lastChild() {
		return children.get(children.size() - 1);
	}

	// increase no of visited node
	public void increaseNumVisited() {
		numVisited++;
	}

	// add node to the tree
	public void addNodeToTree(Node node) {
		children.add(node);
	}



	// Getters for the Node
	public Node getParentNode() {
		return parentNode;
	}

	public List<OthelloMove> getNodeMoves() {
		return moves;
	}

	public OthelloMove getAction() {
		return actionMove;
	}

	public int getNumVisited() {
		return numVisited;
	}

	public OthelloState getBoardState() {
		return boardState;
	}

	public List<Node> getChildren() {
		return children;
	}

	public int getNodeScore() {
		return score;
	}

	// Setters for the Node
	public void setParentNode(Node pNode) {
		parentNode = pNode;
	}

	public void setChildren(List<OthelloMove> cNodes) {
		moves = cNodes;
	}

	public void setAction(OthelloMove m) {
		actionMove = m;
	}

	public void setNumVisited(int num) {
		numVisited = num;
	}


}
