import java.util.List;

public class myEvalDbestPlayer extends OthelloPlayer {

	private int depth;

	public myEvalDbestPlayer(int depth) {
		this.depth = depth;
	}

	@Override
	public OthelloMove getMove(OthelloState state) {
		
		
		//alpha beta pruning variable
		int alpha = Integer.MIN_VALUE;
		int beta = Integer.MAX_VALUE;
		
		// Generate the list of moves 
		List<OthelloMove> moves = state.generateMoves();

		// Array of scores 
		int[] moveScores = new int[moves.size()];

		if (!moves.isEmpty()) {
			// For each move possible from the current state, run miniMax
			for (int i = 0; i < moves.size(); i++) {
				if (state.nextPlayerToMove == 1) {
					moveScores[i] = miniMax(depth, state, false,alpha,beta);
				} else {
					moveScores[i] = miniMax(depth, state, true,alpha,beta);
				}
			}

			//find the best move
			int bestMoveIndex = 0;
			int currentBest = 0;
			for (int k = 0; k < moveScores.length; k++) {

				// Player 2 wants smalles move
				if (state.nextPlayerToMove == 1) {
					if (moveScores[k] < currentBest) {
						currentBest = moveScores[k];
						bestMoveIndex = k;
					}
				} else {
					// Player 1 wants largest move
					if (moveScores[k] > currentBest) {
						currentBest = moveScores[k];
						bestMoveIndex = k;
					}
				}
			}
			return moves.get(bestMoveIndex);
		}

		// If there are no possible move return null
		return null;
	}

	public int miniMax(int depth, OthelloState state, boolean maxPlayer, int a,int b) {
		
		
		//check for depth
		if (depth == 0 || state.gameOver()) {
			return state.myEval();
		}
		if (maxPlayer) {
			int bestScore = -999999;
			// generate all the moves the state has
			List<OthelloMove> moves = state.generateMoves();
			for (OthelloMove m : moves) {
				OthelloState newState = state.applyMoveCloning(m);
				int v = miniMax(depth - 1, newState, false,a,b);
				bestScore = max(bestScore, v);
				if(bestScore >=b)
					return bestScore;
				a=max(a,bestScore);
			}
			return bestScore;
		} else {
			int bestScore = 999999;
			List<OthelloMove> moves = state.generateMoves();
			for (OthelloMove m : moves) {
				OthelloState newState = state.applyMoveCloning(m);
				int v = miniMax(depth - 1, newState, true,a,b);
				bestScore = min(bestScore, v);
				if(bestScore <=a)
					return bestScore;
				b=min(b,bestScore);
			}
			return bestScore;
		}

	}

	//utility functions
	private int min(int best, int v) {
		if (v < best) {
			return v;
		}
		return best;
	}

	private int max(int best, int v) {
		if (v > best) {
			return v;
		}
		return best;
	}
	
}
