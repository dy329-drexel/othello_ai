import java.util.List;

public class DbestPlayer extends OthelloPlayer {

	private int depth;

	// set depth for the search
	public DbestPlayer(int depth) {
		this.depth = depth;
	}

	@Override
	public OthelloMove getMove(OthelloState state) {

		// Generate the list of moves
		List<OthelloMove> moves = state.generateMoves();

		// Array of scores
		int[] moveScores = new int[moves.size()];

		if (!moves.isEmpty()) {
			// For each move possible from the current state, run miniMax
			for (int i = 0; i < moves.size(); i++) {
				if (state.nextPlayerToMove == 1) {
					moveScores[i] = miniMax(depth, state, false);
				} else {
					moveScores[i] = miniMax(depth, state, true);
				}
			}

			// find the best move
			int bestMoveIndex = 0;
			int currentBest = 0;
			for (int k = 0; k < moveScores.length; k++) {

				// Player 2 wants smalles move
				if (state.nextPlayerToMove == 1) {
					if (moveScores[k] < currentBest) {
						currentBest = moveScores[k];
						bestMoveIndex = k;
					}
				} else {
					// Player 1 wants the largest move value
					if (moveScores[k] > currentBest) {
						currentBest = moveScores[k];
						bestMoveIndex = k;
					}
				}
			}
			return moves.get(bestMoveIndex);
		}

		// If there are no possible moves just return null
		return null;
	}

	public int miniMax(int depth, OthelloState state, boolean maxPlayer) {

		// check for depth
		if (depth == 0 || state.gameOver()) {
			return state.score();
		}
		if (maxPlayer) {
			int bestScore = -999999;
			// generate all the moves the state has
			List<OthelloMove> moves = state.generateMoves();
			for (OthelloMove m : moves) {
				OthelloState newState = state.applyMoveCloning(m);
				int v = miniMax(depth - 1, newState, false);
				bestScore = max(bestScore, v);
			}
			return bestScore;
		} else {
			int bestScore = 999999;
			List<OthelloMove> moves = state.generateMoves();
			for (OthelloMove m : moves) {
				OthelloState newState = state.applyMoveCloning(m);
				int v = miniMax(depth - 1, newState, true);
				bestScore = min(bestScore, v);
			}
			return bestScore;
		}

	}

	// utility functions for minimum and max
	private int min(int best, int v) {
		if (v < best) {
			return v;
		}
		return best;
	}

	private int max(int best, int v) {
		if (v > best) {
			return v;
		}
		return best;
	}

}
