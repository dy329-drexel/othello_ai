import java.util.List;

public class DbestTimePlayer extends OthelloPlayer{
	private int time;

	//set time limit
	public DbestTimePlayer(int time) {
		this.time = time;
	}

	@Override
	public OthelloMove getMove(OthelloState state) {
		
		int depth=0;
		//alpha beta pruning variable
		int alpha = Integer.MIN_VALUE;
		int beta = Integer.MAX_VALUE;
		// Generate the list of moves 
		List<OthelloMove> moves = state.generateMoves();

		// Array of scores 
		int[] moveScores = new int[moves.size()];

        
	//check if still have time if have time increase depth by 1
		
		
		if (!moves.isEmpty()) {
			
			long startTime = System.currentTimeMillis();
			long elaspedTime=0;
			
		while(elaspedTime<time){
			depth++;
		
			// For each move possible from the current state, run miniMax
			for (int i = 0; i < moves.size(); i++) {
				if (state.nextPlayerToMove == 1) {
					moveScores[i] = miniMax(depth, state, false,alpha,beta);
				} else {
					moveScores[i] = miniMax(depth, state, true,alpha,beta);
				}
				
			}
			long endTime = System.currentTimeMillis();
			elaspedTime=(endTime-startTime);
		}
		

			/*
			 * Loop through all the scores of the moves to find the best move
			 * for the respective player
			 */
			int bestMoveIndex = 0;
			int currentBest = 0;
			for (int k = 0; k < moveScores.length; k++) {

				// Player X wants the smallest move value possible
				if (state.nextPlayerToMove == 1) {
					if (moveScores[k] < currentBest) {
						currentBest = moveScores[k];
						bestMoveIndex = k;
					}
				} else {
					// Player O wants the largest move value possible
					if (moveScores[k] > currentBest) {
						currentBest = moveScores[k];
						bestMoveIndex = k;
					}
				}
			}
			return moves.get(bestMoveIndex);
		}

		// If there are no possible moves, just return "pass":
		return null;
	}

	public int miniMax(int depth, OthelloState state, boolean maxPlayer, int a,int b) {
		
		
		//check for depth
		if (depth == 0 || state.gameOver()) {
			return state.score();
		}
		if (maxPlayer) {
			int bestScore = -9999999;
			// generate all the moves the state has
			List<OthelloMove> moves = state.generateMoves();
			for (OthelloMove m : moves) {
				OthelloState newState = state.applyMoveCloning(m);
				int v = miniMax(depth - 1, newState, false,a,b);
				bestScore = max(bestScore, v);
				if(bestScore >=b)
					return bestScore;
				a=max(a,bestScore);
			}
			return bestScore;
		} else {
			int bestScore = 999999;
			List<OthelloMove> moves = state.generateMoves();
			for (OthelloMove m : moves) {
				OthelloState newState = state.applyMoveCloning(m);
				int v = miniMax(depth - 1, newState, true,a,b);
				bestScore = min(bestScore, v);
				if(bestScore >=a)
					return bestScore;
				b=max(b,bestScore);
			}
			return bestScore;
		}

	}

	private int min(int best, int v) {
		if (v < best) {
			return v;
		}
		return best;
	}

	private int max(int best, int v) {
		if (v > best) {
			return v;
		}
		return best;
	}
}
