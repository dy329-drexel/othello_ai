

import java.util.ArrayList;
import java.util.List;
import java.util.Random;



public class DbestMctsPlayer extends OthelloPlayer
{
	private int iterations_;
	private Random rand = new Random();	

	//setting iterations
	public DbestMctsPlayer(int iterations)
	{
		iterations_ = iterations;
	}

	public OthelloMove getMove(OthelloState state)
	{
		//Monte CArlo Search algorithm
		Node root = new Node(state);
		if (root.noMoves())
		{
			return null;
		}

		for (int i = 0; i < iterations_; i++)
		{
			Node node = treePolicy(root);
			if (node != null)
			{
				OthelloState node2_state = defaultPolicy(node);
				int node_score = node2_state.score();
				node.backup(node_score);
			}
		}

		return root.bestChild().getAction();
	}	

	
	
	/*If 'node' still has any children that are not in the tree, then it generates one of those children ('newnode'), it
	adds 'newnode' as a child to 'node', and returns 'newnode'.
	If 'node' is a terminal node (no actions can be performed). Then it returns "node"
	If 'node' is not a terminal but all its children are in the tree, then: 90% of the times "nodetmp =
	bestChild(node)", and 10% of the times "nodetmp = [a child of node at random]" (if you are curious, this is
	called an epsilon-greedy strategy). Then, the function returns "treePolicy(nodetmp)"
	*/
	private Node treePolicy(Node node)
	{
		if (node.addNextChild())
		{
			return node.lastChild();
		}

		if (node.gameOver())
		{
			return node;
		}

		Node nodetmp;
		
		if (rand.nextInt(10) < 9)
		{
			nodetmp = node.bestChild();
		}
		else
		{
			nodetmp = node.randomChild(rand);
		}

		return treePolicy(nodetmp);
	}
// this function just uses the random agent to select actions at random for each player, until the
	//game is over, and returns the final state of the game.
	private OthelloState defaultPolicy(Node node)
	{
		OthelloState new_state = node.stateClone();
		while (!new_state.gameOver())
		{
			List<OthelloMove> moves = new_state.generateMoves();
			if (moves.size() > 0)
			{
				new_state.applyMove(moves.get(rand.nextInt(moves.size())));
			}
			else
			{
				new_state.applyMove(null);
			}
		}

		return new_state;
	}
}
